<?php
/**
 * Bondarenko Vadim
 */

return array(

    'user/login' => 'user/login',// loginIndex в UserController
    'user/logout' => 'user/logout',// logoutIndex в UserController
    'user/register' => 'user/register',// registerIndex в UserController
    'task2' => 'task2/task',
    
    'cabinet' => 'cabinet/index',
    
    '' => 'site/index', // actionIndex в SiteController
    
);