-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Вер 03 2017 р., 23:20
-- Версія сервера: 5.6.37
-- Версія PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `test1`
--

-- --------------------------------------------------------

--
-- Структура таблиці `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`) VALUES
(1, 'Аудіо файли', 0),
(2, 'Відео файли', 0),
(3, 'Наші', 1),
(4, 'Закордонні', 1),
(5, 'Наші', 2),
(6, 'Закордонні', 2),
(7, 'Книги', 3),
(8, 'Музика', 3),
(9, 'Тренінги', 3),
(10, 'Тренінги', 4),
(11, 'Драма', 6),
(12, 'Тренінг', 6),
(13, 'Бойовик', 6),
(14, 'Фантастика', 4),
(15, 'Бойовик', 4);

-- --------------------------------------------------------

--
-- Структура таблиці `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `products`
--

INSERT INTO `products` (`id`, `name`, `price`) VALUES
(1, 'Гондопас: Камасутра для оратора', '200'),
(2, 'Браян Трейси: Искуство заключать сделки', '79'),
(3, 'Начало', '100'),
(4, 'Думай и Богатей', '169'),
(5, 'Убить Билла', '159'),
(6, 'Самураи', '99'),
(7, 'Форсаж1', '175'),
(8, 'Мастер и Маргарита', '184'),
(9, 'Сердце Воина', '178'),
(10, 'Ораторское кунг-фу', '64'),
(11, 'Сбоник песен 90-х', '86');

-- --------------------------------------------------------

--
-- Структура таблиці `product_to_categories`
--

CREATE TABLE `product_to_categories` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `categ_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `product_to_categories`
--

INSERT INTO `product_to_categories` (`product_id`, `categ_id`) VALUES
(1, 7),
(1, 9),
(2, 7),
(2, 12),
(2, 19),
(3, 13),
(4, 10),
(4, 12),
(5, 13),
(6, 13),
(7, 13),
(8, 7),
(8, 11),
(9, 15),
(9, 10),
(10, 12),
(10, 10),
(11, 8),
(1, 12);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`) VALUES
(4, 'Vova', '', '123456', 1),
(5, 'Vadim', '111@ukr.net', '$2y$10$K4LU7sMernV9lND.5DuopOBJJNoGV91eLXkdoUSC1QhV/slOKqeZq', 1),
(6, 'Vadim', '222@gmail.com', '$2y$10$JRLJacGw1gIhkL7eF4UVTeVwdB8nYclTFNkcy5i4F4Mi.4n5dAGoS', 1),
(7, 'Vadim', '333@ukr.net', '$2y$10$ff8GMzpaleO4G755kAuJ.e3FHUJvda8u6xPNPvgTKkF9FUsWghoy.', 1);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблиці `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
