<?php

class UserController
{
    
     public function actionRegister()
    {
        $name = false;
        $email = false;
        $password = false;
        $result = false;

        // Form processing
        if (null !== filter_input(INPUT_POST, 'submit')) {
            // If the form is sent get the data from the form

            $name = filter_input(INPUT_POST, 'name');
            $email = filter_input(INPUT_POST, 'email');
            $passwordHash = filter_input(INPUT_POST, 'password');
            $password = password_hash($passwordHash, PASSWORD_DEFAULT);

            // Error flag
            $errors = false;

            // validation of fields
            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }

            if (User::checkEmailExists($email)) {
                $errors[] = 'Такой email уже используется';
            }
            
            if ($errors == false) {
                // If there are no errors, we register the user
                $result = User::register($name, $email, $password);
            }
        }

        require_once(ROOT . '/views/user/register.php');
        return true;
    }

    public function actionLogin()
    {
        $name = '';
        //$password = '';
       
        
               
        if (null !== filter_input(INPUT_POST, 'submit')) {
            $name = filter_input(INPUT_POST, 'name');
            $password = filter_input(INPUT_POST, 'password');

            $errors = false;
                   
            // validation of fields
            if (!User::checkName($name)) {
                $errors[] = 'Неверные данные - имя';
            }            

            // check if the user exists
            $userId = User::checkUserData($name, $password);
            
             
            if ($userId == false) {
                // If the data is incorrect - show an error
                
                $_SESSION["total"]++;//unsuccessful form dispatch counter
                //I can make a counter through the database remembering the user's IP
                                
                if (isset($_SESSION["total"]) && $_SESSION["total"] < 3) {
                    $errors[] = 'Неверные данные';
                } else {     
                    $hidden = 'hidden';
                    setcookie('hidden', $hidden, time() + 15 );            
                }

               
                //print_r($_SESSION["total"]); 

            } else {
                // If the data is correct, remember the user (session)
                User::auth($userId);
                
                // Redirecting the user to the closed part - the user's cabinet
                header("Location: /cabinet/"); 
            }

        } 
        else {
            $_SESSION["total"] = 0;
        }
        

        require_once(ROOT . '/views/user/login.php');

        return true;
    }
    
    /**
     * Delete user data from session
     */
    public function actionLogout()
    {
        unset($_SESSION["user"]);
        header("Location: /user/login");
    }
  

}
