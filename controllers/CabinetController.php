<?php
/**
 * Bondarenko Vadim
 */

class CabinetController
{

    public function actionIndex()
    {
        /**
         * Get user ID from session
         */
        $userId = User::checkLogged();
        
        $user = User::getUserById($userId);
                
        require_once(ROOT . '/views/cabinet/index.php');

        return true;
    }  
    
    

}