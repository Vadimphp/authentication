<?php
/**
 * Bondarenko Vadim
 */

class User
{

    /**
     * User registration
     * @param string $name 
     * @param string $email 
     * @param string $password 
     * @return boolean 
     */
    public static function register($name, $email, $password)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO user (name, email, password) '
                . 'VALUES (:name, :email, :password)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        
        return $result->execute();
    }
    
    /**
     * Check if the user exists with the specified $name and $password 
     */
    public static function checkUserData($name, $password)
    {
        
        // connecting to the database
        $db = Db::getConnection();

       
        $sql = 'SELECT * FROM user WHERE name = :name';
        
        // use the prepared query
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_INT);
        //$result->bindParam(':password', $password, PDO::PARAM_INT);
        $result->execute();

        $users = $result->fetchAll();
        //$user = $result->fetch();

        foreach ($users as $user) {

            if (password_verify($password, $user['password']) ) {
                return $user['id'];
            }

        }

    }

    /**
     * Remember user
     * @param string $name
     * @param string $password
     */
    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }
    
    public static function checkLogged()
    {
        //If there is a session, return the user ID
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        header("Location: /user/login");
    }
    /**
     * If the guest, then we'll just click the Login button, if not, then the Cabinet and Exit
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    /**
     * Checks the name: no less than 2 characters 
     */
    public static function checkName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        }
        return false;
    }
    
    public static function getUserById($id)
    {

        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // data in an array
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }
    

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
    
    public static function checkEmailExists($email)
    {      
        $db = Db::getConnection();

        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }
    
    
    // Task 2 
    public static function breadcrumb($id) {
        
        $db = Db::getConnection();

        $sql = 'SELECT id, name, parent_id FROM categories WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->execute();
        $u = $result->fetch();
        
        //echo '<pre>';
        //print_r($u);
        //echo '</pre>';
        //die;
        
        if ($u['parent_id'] != 0) {
            return User::breadcrumb($u['id']) . " -> " . $u['name'];
            //return $u['name'];
        } else {
            return $u['name'];
        }
    }

}
