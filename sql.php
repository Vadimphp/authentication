<?php


//Table structure for table `categories`

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `parent_id` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 16;



//Dumping data for table `categories`

INSERT INTO `categories` (`id`, `name`, `parent_id`) VALUES

(1, 'Аудіо файли', 0),
 (2, 'Відео файли', 0),
 (3, 'Наші', 1),
 (4, 'Закордонні', 1),
 (5, 'Наші', 2),
 (6, 'Закордонні', 2),
 (7, 'Книги', 3),
 (8, 'Музика', 3),
 (9, 'Тренінги', 3),
 (10, 'Тренінги', 4),
 (11, 'Драма', 6),
 (12, 'Тренінг', 6),
 (13, 'Бойовик', 6),
 (14, 'Фантастика', 4),
 (15, 'Бойовик', 4);



//Table structure for table `products`

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `price` varchar(20) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 12;


INSERT INTO `products` (`id`, `name`, `price`) VALUES
(1, 'Гондапас: Камасутра для оратора', '200'),
 (2, 'Браян Трейси: Искуство заключать сделки', '79'),
 (3, 'Начало', '100'),
 (4, 'Думай и Богатей', '169'),
 (5, 'Убить Билла', '159'),
 (6, 'Самураи', '99'),
 (7, 'Форсаж1', '175'),
 (8, 'Мастер и Маргарита', '184'),
 (9, 'Сердце Воина', '178'),
 (10, 'Ораторское кунг-фу', '64'),
 (11, 'Сбоник песен 90-х', '86');


--
--Table structure for table `product_to_categories`
--
CREATE TABLE IF NOT EXISTS `product_to_categories` (
`product_id` int(11) NOT NULL DEFAULT '0',
 `categ_id` int(11) NOT NULL DEFAULT '0'
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


--Dumping data for table `product_to_categories`
--
INSERT INTO `product_to_categories` (`product_id`, `categ_id`) VALUES
 (1, 7),
 (1, 9),
 (2, 7),
 (2, 12),
 (2, 19),
 (3, 13),
 (4, 10),
 (4, 12),
 (5, 13),
 (6, 13),
 (7, 13),
 (8, 7),
 (8, 11),
 (9, 15),
 (9, 10),
 (10, 12),
 (10, 10),
 (11, 8),
 (1, 12);



/* 	a. Для заданного списка товаров получить названия всех категорий, в которых представлены товары; 
  products - 1, 3, 
 */
SELECT DISTINCT ptc.product_id, ct.name, p.name FROM categories ct
LEFT JOIN product_to_categories ptc ON ptc.categ_id = ct.id
INNER JOIN products AS p ON ptc.product_id = p.id
WHERE ptc.product_id IN('1', '2')



/* 	b. Для заданной категории получить список предложений всех товаров из этой категории и ее дочерних категорий;
  category - 7
 */
SELECT p.id, p.name FROM products p
LEFT JOIN product_to_categories ptc ON ptc.product_id = p.id
WHERE ptc.categ_id = '7'
UNION
SELECT p.id, p.name FROM products p
LEFT JOIN product_to_categories ptc ON ptc.product_id = p.id
WHERE ptc.categ_id IN(SELECT id FROM categories WHERE parent_id = '0')



/* 	c. Для заданного списка категорий получить количество предложений товаров в каждой категории; 
  categories '7','9', '10'
 */
SELECT categ_id, COUNT(product_id) p_count FROM product_to_categories
WHERE categ_id IN('9', '10', '7') GROUP BY categ_id




/* 	d. Для заданного списка категорий получить общее количество уникальных предложений товара;
  categories '13', '15', '16', '17'
 */
SELECT SUM(p_count) prod_sum FROM (
SELECT ptc.categ_id, COUNT(product_id) p_count FROM product_to_categories ptc
WHERE ptc.categ_id IN('13', '15', '16', '17')
AND ptc.product_id NOT IN (
SELECT sub_ptc.product_id FROM product_to_categories sub_ptc
WHERE sub_ptc.categ_id IN('12', '11', '7', '14') AND sub_ptc.categ_id != ptc.categ_id
) GROUP BY categ_id ) t1


